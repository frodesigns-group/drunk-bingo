require("./manifest.json");
require("./assets/img/shamrock.png");

import "./assets/global-css/reset.scss";
import "./assets/global-css/general.scss";
import "./assets/global-css/typography.scss";
import "./assets/global-css/forms.scss";

import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from "vuex";
import VueCookie from "vue-cookie";
import config from "./config.js";
import routes from "./routes.js";
import global from "./global.js";
import components from "./reusable-components.js";
import App from "./App.vue";

Vue.use(VueCookie);

// routing
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
        offset: { x: 0, y: 52 }
      };
    } else {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (savedPosition) {
            resolve(savedPosition);
          } else {
            resolve({ x: 0, y: 0 });
          }
        }, 300);
      });
    }
  }
});

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title + " - " + config.siteName;
  }
  if (
    window.innerWidth <= 1024 &&
    document.getElementById("app").classList.contains("nav--open")
  ) {
    store.commit("toggleNav");
  }
  next();
});

router.onReady(function() {
  setTimeout(function() {
    if (router.currentRoute.hash) {
      var el = document.getElementById(
        router.currentRoute.hash.replace("#", "")
      );
      var yPos = el.offsetTop - 52;
      window.scrollTo(0, yPos);
    }
  }, 25);

  if (router.currentRoute.meta.title) {
    document.title = router.currentRoute.meta.title + " - " + config.siteName;
  }
});

// state
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    navOpen: false,
    headerHidden: false,
    darkTheme: false,
    primaryColor: config.primaryColor,
    secondaryColor: config.secondaryColor,
    colorChoices: config.colorChoices
  },
  mutations: {
    toggleNav(state) {
      state.navOpen = !state.navOpen;
      document
        .getElementsByTagName("body")[0]
        .classList.toggle("body--no-scroll");
    },
    hideHeader(state) {
      state.headerHidden = true;
    },
    showHeader(state) {
      state.headerHidden = false;
    },
    toggleTheme(state) {
      state.darkTheme = !state.darkTheme;
    },
    setPrimaryColor(state, color) {
      state.primaryColor = color;
    },
    setSecondaryColor(state, color) {
      state.secondaryColor = color;
    }
  }
});

// app main vue instance
const app = new Vue({
  el: "#app",
  store,
  template: "<App/>",
  components: { App },
  router: router
});

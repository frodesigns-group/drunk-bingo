document.addEventListener('DOMContentLoaded', function() {
  inputValueListener()
})

function inputValueListener() {
  var appEl = document.getElementById("app")

  appEl.addEventListener("keyup", function(e) {
    if (e.target.matches('input:not([type="checkbox"]):not([type="radio"]), textarea')) {
      if (e.target.value == "") {
        e.target.classList.remove("input--has-value");
      } else {
        e.target.classList.add("input--has-value");
      }
    }
  })
}

const r404 = () => import("./components/main-routes/404.vue");
const rHome = () => import("./components/main-routes/home.vue");

const routes = [
  { path: "*", component: r404, meta: { title: "Page Not Found" } },
  {
    path: "/",
    component: rHome,
    meta: { title: "Play" }
  }
];

export default routes;
